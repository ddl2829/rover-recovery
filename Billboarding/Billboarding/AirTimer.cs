using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace Billboarding
{
    /// <summary>
    /// This is a game component that implements IUpdateable.
    /// </summary>
    public class AirTimer : Microsoft.Xna.Framework.DrawableGameComponent
    {
        public TimeSpan TimeLeft = TimeSpan.Zero;
        TimeSpan TimeAllowed = TimeSpan.FromMinutes(2.0f);
        public event EventHandler OnTimeUp;
        SpriteBatch spriteBatch;
        Texture2D timerTexture;

        public AirTimer(Game game, SpriteBatch _spriteBatch)
            : base(game)
        {
            spriteBatch = _spriteBatch;
            TimeLeft = TimeAllowed;

            ContentManager Content = (ContentManager)Game.Services.GetService(typeof(ContentManager));
            timerTexture = Content.Load<Texture2D>(@"Sprites\pixel");
        }

        /// <summary>
        /// Allows the game component to perform any initialization it needs to before starting
        /// to run.  This is where it can query for any required services and load content.
        /// </summary>
        public override void Initialize()
        {
            base.Initialize();
        }

        /// <summary>
        /// Allows the game component to update itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        public override void Update(GameTime gameTime)
        {
            TimeLeft -= gameTime.ElapsedGameTime;
            if (TimeLeft < TimeSpan.Zero)
                OnTimeUp(this, null);
            base.Update(gameTime);
        }

        public override void Draw(GameTime gameTime)
        {
            spriteBatch.Begin();

            spriteBatch.Draw(timerTexture, new Rectangle(8, 8, 154, 44), Color.ForestGreen);
            spriteBatch.Draw(timerTexture, new Rectangle(10, 10, 150, 40), Color.White);
            spriteBatch.Draw(timerTexture, new Rectangle(10, 10, (int)(TimeLeft.TotalMilliseconds/TimeAllowed.TotalMilliseconds * 150), 40), Color.CornflowerBlue);

            spriteBatch.End();

            base.Draw(gameTime);
        }
    }
}
