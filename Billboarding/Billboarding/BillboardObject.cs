﻿using System;
using Billboarding.Components;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Billboarding
{
    public class BillboardObject
    {
        public Vector2 Position;
        public Vector2 Direction;
        public int CurrentTexture = 0;
        private static Texture2D[] textures;

        public BillboardObject(Vector2 Position, Vector2 Direction)
        {
            this.Position = Position;
            this.Direction = Direction;
        }

        /// <summary>
        /// Sets the current texture based on the angle to the camera
        /// </summary>
        /// <param name="camera">The camera to calculate the texture based on</param>
        public void UpdateCurrentTexture(Camera camera)
        {
            Vector2 toCamera = camera.VectorToCamera(Position);
            toCamera.Normalize();
            double angle = MathHelper.ToDegrees((float)Math.Acos(Vector2.Dot(Direction, toCamera)));
            CurrentTexture = (int)Math.Floor(angle / 10);
        }

        /// <summary>
        /// Set the object textures for the class
        /// </summary>
        /// <param name="_textures">The array of textures to use</param>
        public void setTextures(Texture2D[] _textures)
        {
            textures = _textures;
        }

        /// <summary>
        /// Convert the position to a unit vector for the minimap
        /// </summary>
        public Vector2 UnitVector
        {
            get { return new Vector2(Position.X / 20, Position.Y / 20); }
        }

        public Texture2D Texture
        {
            get { return textures[CurrentTexture]; }
        }
    }
}
