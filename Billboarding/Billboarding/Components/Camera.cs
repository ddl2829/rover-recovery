﻿using System;
using System.Diagnostics;
using Malibu.Input;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

namespace Billboarding.Components
{
    public class Camera : GameComponent
    {
        public event EventHandler OnMove;
        
        public Vector2 Position;
        public Vector2 NextPosition;
        public Vector2 Direction;

        public bool nextMoveForward = true;
        
        public float FOV = 90.0f;

        public float cameraSpeed = 0.05f;
        float rotationSpeed = 1.0f;

        /// <summary>
        /// Returns the vector perpendicular to the direction vector
        /// </summary>
        public Vector2 PerpendicularDirection
        {
            get { return new Vector2(-Direction.Y, Direction.X); }
        }

        /// <summary>
        /// Returns the position as a unit vector of the game plane
        /// </summary>
        public Vector2 UnitPosition
        {
            get { return Position / 20; }
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="game">The Game</param>
        public Camera(Game1 game) : base(game)
        {
            Position = new Vector2(10, 10);
            Direction = new Vector2(0, -1);
            Direction.Normalize();
        }


        /// <summary>
        /// Set the camera's position
        /// </summary>
        /// <param name="_position">The New position to set the camera to</param>
        /// <param name="_direction">The vector the camera is facing</param>
        public void SetPosition(Vector2 _position, Vector2 _direction)
        {
            Position = _position;
            Direction = _direction;
            Direction.Normalize();
        }

        public override void Update(GameTime gameTime)
        {
            if (InputHandler.MouseMoved())
            {
                float rotation = InputHandler.MouseMotionFromLock().X * rotationSpeed;
                rotation = MathHelper.WrapAngle(MathHelper.ToRadians(rotation));
                Direction = Vector2.Transform(Direction, Matrix.CreateRotationZ(rotation));
                Direction.Normalize();
                InputHandler.LockMouse();
            }
            Move();
            base.Update(gameTime);
        }

        /// <summary>
        /// Prepare camera movement, triggers OnMove event for the screen to perform actions
        /// Movement itself is applied in the game screen
        /// </summary>
        private void Move()
        {
            NextPosition = Position;
            nextMoveForward = false;
            if (InputHandler.KeyDown(Keys.W))
            {
                nextMoveForward = true;
                NextPosition += Direction * cameraSpeed;
            }
            if (InputHandler.KeyDown(Keys.S))
                NextPosition -= Direction * cameraSpeed;
            if (InputHandler.KeyDown(Keys.A))
                NextPosition -= PerpendicularDirection * cameraSpeed;
            if (InputHandler.KeyDown(Keys.D))
                NextPosition += PerpendicularDirection * cameraSpeed;
            OnMove(this, null);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Target"></param>
        /// <returns></returns>
        public Vector2 VectorToTarget(Vector2 Target)
        {
            return Target - Position;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Origin"></param>
        /// <returns></returns>
        public Vector2 VectorToCamera(Vector2 Origin)
        {
            return Position - Origin;
        }

        /// <summary>
        /// Check if the target is in the camera's field of view
        /// </summary>
        /// <param name="Target">The target to check</param>
        /// <returns>bool - true if in FOV, false if not</returns>
        public bool InFOV(Vector2 Target)
        {
            Vector2 ToTarget = VectorToTarget(Target);
            ToTarget.Normalize();
            double angle = Math.Acos(Vector2.Dot(Direction, ToTarget));
            return angle <= MathHelper.ToRadians(FOV/2);
        }

        /// <summary>
        /// Maps the target's location to the screen X coordinate
        /// Method based on code from a raycasting engine, found here: https://www.allegro.cc/forums/thread/355015
        /// </summary>
        /// <param name="Target">The target to map</param>
        /// <param name="screenRect">The screen rectangle</param>
        /// <returns>float the X location for the center of the object</returns>
        public float ScreenXCoord(Vector2 Target, Rectangle screenRect)
        {
            Vector2 toTarget = VectorToTarget(Target);
            double theta = Math.Atan2(Direction.X, Direction.Y);
            double d_theta = MathHelper.ToDegrees((float)theta);
            double thetaTemp = Math.Atan2(toTarget.X, toTarget.Y);
            double d_thetaTemp = MathHelper.ToDegrees((float)thetaTemp);
            if (d_thetaTemp < 0)
                d_thetaTemp += 360;
            double yTmp = d_theta + FOV/2 - d_thetaTemp;
            if (d_thetaTemp > 270 && d_theta < 90)
                yTmp = d_theta + FOV / 2 - d_thetaTemp + 360;
            if (d_theta > 270 && d_thetaTemp < 90)
                yTmp = d_theta + FOV / 2 - d_thetaTemp - 360;
            if (d_thetaTemp < 270 && d_theta < 0)
                yTmp = d_theta + FOV / 2 - d_thetaTemp + 360;
            float xTmp = (float)yTmp * screenRect.Width / FOV;
            return xTmp;
        }

        /// <summary>
        /// Tells the scale for images
        /// </summary>
        /// <param name="Target">Location of the target to get a scale factor for</param>
        /// <returns>float the scale factor for the target's texture</returns>
        public float TextureScale(Vector2 Target)
        {
            float scale = (float)2 / MathHelper.Max(Math.Abs(Position.X - Target.X), Math.Abs(Position.Y - Target.Y));
            return scale;
        }
    }
}
