using Billboarding.GameScreens;
using Malibu.GameStates;
using Malibu.Input;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace Billboarding
{
    /// <summary>
    /// This is the main type for your game
    /// </summary>
    public class Game1 : Microsoft.Xna.Framework.Game
    {
        GraphicsDeviceManager graphics;
        public SpriteBatch spriteBatch;

        public GameStateManager GSManager;

        //GameScreens
        public TitleScreen TitleScreen;
        public HelpScreen HelpScreen;
        public ActionScreen ActionScreen;
        public GameOverScreen GameOverScreen;

        //Game background music
        public Song backgroundMusic;

        //Set screen resolution
        public readonly Rectangle ScreenRectangle = new Rectangle(0, 0, 800, 600);

        Texture2D background;

        public Game1()
        {
            graphics = new GraphicsDeviceManager(this);
            Window.Title = "Rover Recovery";
            graphics.PreferredBackBufferWidth = ScreenRectangle.Width;
            graphics.PreferredBackBufferHeight = ScreenRectangle.Height;
            Content.RootDirectory = "Content";
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            //Add ContentManager and spritebatch to the game's services
            Services.AddService(typeof(ContentManager), Content);

            spriteBatch = new SpriteBatch(GraphicsDevice);
            Services.AddService(typeof(SpriteBatch), spriteBatch);

            //Add the input manager and game state manager
            Components.Add(new InputHandler(this));
            GSManager = new GameStateManager(this);
            Components.Add(GSManager);            

            //Initialize the game screens
            TitleScreen = new TitleScreen(this, GSManager);
            HelpScreen = new HelpScreen(this, GSManager);
            ActionScreen = new ActionScreen(this, GSManager);
            GameOverScreen = new GameOverScreen(this, GSManager);

            //Set the initial game screen
            GSManager.ChangeState(TitleScreen);

            base.Initialize();
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {            
            //Load the 1 pixel background and background music
            background = Content.Load<Texture2D>(@"Sprites\pixel");
            backgroundMusic = Content.Load<Song>(@"Sounds\backgroundmusic");
            //Set music to loop and begin playing
            MediaPlayer.IsRepeating = true;
            MediaPlayer.Play(backgroundMusic);
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// all content.
        /// </summary>
        protected override void UnloadContent()
        {
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            // Allows the game to exit
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || InputHandler.KeyPressed(Keys.Escape))
                this.Exit();
            base.Update(gameTime);
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.SandyBrown);
            spriteBatch.Begin();
            spriteBatch.Draw(background, new Rectangle(0, ScreenRectangle.Height/2, ScreenRectangle.Width, ScreenRectangle.Height), Color.IndianRed);
            spriteBatch.End();
            base.Draw(gameTime);
        }
    }
}
