﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using Billboarding.Components;
using Malibu.GameStates;
using Malibu.Input;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Audio;

namespace Billboarding.GameScreens
{
    public class ActionScreen : BaseGameScreen
    {
        Camera camera;
        SpriteBatch spriteBatch;

        List<BillboardObject> Rovers;
        MiniMap map;
        AirTimer timer;

        bool salvaging = false;
        TimeSpan timeSalvaged;
        TimeSpan TimeToSalvage = TimeSpan.FromSeconds(3.0f);
        Texture2D salvageTimerTexture;
        SpriteFont scoreFont;

        SoundEffect salvageSound;
        SoundEffectInstance soundPlaying;

        public int Score = 0;
        public bool Dead = false;

        public ActionScreen(Game1 game, GameStateManager manager)
            : base(game, manager)
        {
        }

        public override void Initialize()
        {
            spriteBatch = (SpriteBatch)GameRef.Services.GetService(typeof(SpriteBatch));
            camera = new Camera(GameRef);
            camera.OnMove += new EventHandler(camera_onMove);
            Components.Add(camera);
            map = new MiniMap(GameRef, new Rectangle(GameRef.ScreenRectangle.Width - 200, GameRef.ScreenRectangle.Height - 200, 200, 200), GameRef.spriteBatch);
            map.camera = camera;
            Components.Add(map);
            timer = new AirTimer(GameRef, GameRef.spriteBatch);
            timer.OnTimeUp += new EventHandler(timer_onTimeUp);
            Components.Add(timer);

            Rovers = new List<BillboardObject>();

            base.Initialize();
        }

        private void timer_onTimeUp(object sender, EventArgs e)
        {
            Dead = true;
            StateManager.ChangeState(GameRef.GameOverScreen);
        }

        private void camera_onMove(object sender, EventArgs e)
        {
            foreach (BillboardObject rover in Rovers)
            {
                if (Vector2.Distance(camera.NextPosition, rover.Position) < 1)
                {
                    if (camera.nextMoveForward == true)
                    {
                        PushRover(rover);
                    }
                    else
                    {
                        return;
                    }
                }
            }
            if (camera.NextPosition.X < 0 || camera.NextPosition.X > 20 || camera.NextPosition.Y < 0 || camera.NextPosition.Y > 20)
                return;
            camera.Position = camera.NextPosition;
        }

        private void PushRover(BillboardObject rover)
        {
            rover.Position += camera.Direction * camera.cameraSpeed;
        }

        protected override void LoadContent()
        {
            ContentManager Content = (ContentManager)GameRef.Services.GetService(typeof(ContentManager));
            Texture2D[] RoverTextures = new Texture2D[36];
            for (int i = 0; i < 36; ++i)
            {
                RoverTextures[i] = Content.Load<Texture2D>(@"Sprites\Rover\rover00" + i);
            }
            PopulateRovers();
            Rovers[0].setTextures(RoverTextures);
            salvageTimerTexture = Content.Load<Texture2D>(@"Sprites\pixel");

            scoreFont = Content.Load<SpriteFont>(@"Fonts\ControlFont");

            salvageSound = Content.Load<SoundEffect>(@"Sounds\salvage");

            map.SetObjects(ref Rovers);
            base.LoadContent();
        }

        private void PopulateRovers()
        {
            Random rand = new Random();
            for (int i = Rovers.Count; i < 20; ++i)
                Rovers.Add(new BillboardObject(new Vector2(rand.Next(20), rand.Next(20)), new Vector2(0, 1)));
        }

        public override void Update(GameTime gameTime)
        {
            if (InputHandler.KeyDown(Keys.Space))
            {
                List<BillboardObject> nearbyObjects = Rovers.FindAll(rover => Vector2.Distance(camera.Position, rover.Position) <= 1.5);

                if (salvaging == false && nearbyObjects.Count > 0)
                {
                    salvaging = true;
                    timeSalvaged = TimeToSalvage;
                    soundPlaying = salvageSound.CreateInstance();
                    soundPlaying.Play();
                }
                else if (salvaging == true)
                {
                    timeSalvaged -= gameTime.ElapsedGameTime;
                    if (timeSalvaged < TimeSpan.Zero)
                    {
                        Score += nearbyObjects.Count * (int)Math.Floor(timer.TimeLeft.TotalMilliseconds)/10;
                        foreach (BillboardObject closeObject in nearbyObjects)
                            Rovers.Remove(closeObject);
                        salvaging = false;
                        soundPlaying.Stop();
                    }
                }
            }
            else if (InputHandler.KeyReleased(Keys.Space))
            {
                salvaging = false;
                soundPlaying.Stop();
            }

            if (Rovers.Count == 0)
                StateManager.ChangeState(GameRef.GameOverScreen);

            foreach (BillboardObject rover in Rovers)
                rover.UpdateCurrentTexture(camera);

            base.Update(gameTime);
        }

        public override void Draw(GameTime gameTime)
        {
            GameRef.spriteBatch.Begin(SpriteSortMode.FrontToBack, BlendState.AlphaBlend);
            foreach (BillboardObject rover in Rovers)
            {
                if (camera.InFOV(rover.Position))
                {
                    Rectangle ScreenPosition = new Rectangle(
                        (int)(camera.ScreenXCoord(rover.Position, GameRef.ScreenRectangle) - (camera.TextureScale(rover.Position) * rover.Texture.Width)/2), 
                        (int)((GameRef.ScreenRectangle.Height - (rover.Texture.Height * camera.TextureScale(rover.Position))) / 2), 
                        (int)(rover.Texture.Width * camera.TextureScale(rover.Position)), 
                        (int)(rover.Texture.Height * camera.TextureScale(rover.Position)));
                    GameRef.spriteBatch.Draw(rover.Texture, ScreenPosition, null, Color.White, 0, Vector2.Zero, SpriteEffects.None, 1/Vector2.Distance(camera.Position, rover.Position));
                }
            }
            if (salvaging)
            {
                GameRef.spriteBatch.Draw(salvageTimerTexture, new Rectangle((GameRef.ScreenRectangle.Width - 100) / 2, GameRef.ScreenRectangle.Height / 3, 100, 30), null, Color.ForestGreen, 0, Vector2.Zero, SpriteEffects.None, .99f);
                GameRef.spriteBatch.Draw(salvageTimerTexture, new Rectangle((GameRef.ScreenRectangle.Width - 96) / 2, GameRef.ScreenRectangle.Height / 3 + 2, 96, 26), null, Color.White, 0, Vector2.Zero, SpriteEffects.None, .999f);
                GameRef.spriteBatch.Draw(salvageTimerTexture, new Rectangle((GameRef.ScreenRectangle.Width - 96) / 2, GameRef.ScreenRectangle.Height / 3 + 2, (int)(timeSalvaged.TotalMilliseconds/TimeToSalvage.TotalMilliseconds * 96), 26), null, Color.Red, 0, Vector2.Zero, SpriteEffects.None, .9999f);
            }
            string score = String.Format("Score: {0}", Score);
            GameRef.spriteBatch.DrawString(scoreFont, score, new Vector2(GameRef.ScreenRectangle.Width - scoreFont.MeasureString(score).X - 20, 10), Color.White, 0, Vector2.Zero, 1, SpriteEffects.None, .99999f);

            GameRef.spriteBatch.End();
            base.Draw(gameTime);
        }
    }
}
