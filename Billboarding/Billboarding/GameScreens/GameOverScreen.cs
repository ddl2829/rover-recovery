﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Malibu.GameStates;
using Microsoft.Xna.Framework;
using Malibu.Controls;
using Microsoft.Xna.Framework.Graphics;

namespace Billboarding.GameScreens
{
    public class GameOverScreen : BaseGameScreen
    {
        SpriteFont infoFont;
        LinkLabel newGame;
        LinkLabel titleScreen;
        LinkLabel exitGame;

        public GameOverScreen(Game1 game, GameStateManager manager)
            : base(game, manager)
        {

        }

        public override void Initialize()
        {
            base.Initialize();
        }

        protected override void LoadContent()
        {
            base.LoadContent();
            infoFont = GameRef.Content.Load<SpriteFont>(@"Fonts\TitleFont");
            newGame = new LinkLabel();
            titleScreen = new LinkLabel();
            exitGame = new LinkLabel();

            newGame.Text = "New Game";
            newGame.Position = new Vector2((GameRef.ScreenRectangle.Width - newGame.SpriteFont.MeasureString(newGame.Text).X) / 2, 2*GameRef.ScreenRectangle.Height / 3);
            newGame.HasFocus = true;
            newGame.Selected += new EventHandler(newGame_Selected);

            titleScreen.Text = "Return to Title";
            titleScreen.Position = new Vector2((GameRef.ScreenRectangle.Width - titleScreen.SpriteFont.MeasureString(titleScreen.Text).X) / 2, newGame.Position.Y + 30);
            titleScreen.HasFocus = false;
            titleScreen.Selected += new EventHandler(titleScreen_Selected);

            exitGame.Text = "Exit Game";
            exitGame.Position = new Vector2((GameRef.ScreenRectangle.Width - exitGame.SpriteFont.MeasureString(exitGame.Text).X) / 2, titleScreen.Position.Y + 30);
            exitGame.HasFocus = false;
            exitGame.Selected += new EventHandler(exitGame_Selected);

            ControlManager.Add(newGame);
            ControlManager.Add(titleScreen);
            ControlManager.Add(exitGame);
        }

        void titleScreen_Selected(object sender, EventArgs e)
        {
            ResetActionScreen();
            StateManager.ChangeState(GameRef.TitleScreen);
        }

        private void ResetActionScreen()
        {
            GameRef.ActionScreen = new ActionScreen(GameRef, StateManager);
        }

        void exitGame_Selected(object sender, EventArgs e)
        {
            GameRef.Exit();
        }

        void newGame_Selected(object sender, EventArgs e)
        {
            ResetActionScreen();
            StateManager.ChangeState(GameRef.ActionScreen);
        }

        public override void Update(GameTime gameTime)
        {
            ControlManager.Update(gameTime);
            base.Update(gameTime);
        }

        public override void Draw(GameTime gameTime)
        {
            GameRef.spriteBatch.Begin();
            string win_loss = GameRef.ActionScreen.Dead ? "You ran out of oxygen!" : "Escaped from Mars successfully!";
            GameRef.spriteBatch.DrawString(infoFont, win_loss, new Vector2((GameRef.ScreenRectangle.Width - infoFont.MeasureString(win_loss).X) / 2, GameRef.ScreenRectangle.Height / 4), Color.White);
            GameRef.spriteBatch.DrawString(infoFont, String.Format("Score: {0}", GameRef.ActionScreen.Score), new Vector2((GameRef.ScreenRectangle.Width - infoFont.MeasureString(String.Format("Score: {0}", GameRef.ActionScreen.Score)).X) / 2, GameRef.ScreenRectangle.Height / 3), Color.White);
            ControlManager.Draw(GameRef.spriteBatch);
            GameRef.spriteBatch.End();
            base.Draw(gameTime);
        }
    }
}
