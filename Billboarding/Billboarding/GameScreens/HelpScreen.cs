﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Malibu.GameStates;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using Malibu.Controls;

namespace Billboarding.GameScreens
{
    public class HelpScreen : BaseGameScreen
    {
        Texture2D background;
        LinkLabel backLabel;

        public HelpScreen(Game1 game, GameStateManager manager)
            : base(game, manager)
        {

        }

        public override void Initialize()
        {
            base.Initialize();
        }

        protected override void LoadContent()
        {
            base.LoadContent();
            background = GameRef.Content.Load<Texture2D>(@"Help");
            backLabel = new LinkLabel();
            backLabel.Text = "Press Enter to Return";
            backLabel.Color = Color.White;
            backLabel.HasFocus = true;
            backLabel.Position = new Vector2(GameRef.ScreenRectangle.Width - backLabel.SpriteFont.MeasureString(backLabel.Text).X - 20, GameRef.ScreenRectangle.Height - 30);
            backLabel.Selected += new EventHandler(backLabel_selected);
            ControlManager.Add(backLabel);
        }

        internal void backLabel_selected(object sender, EventArgs e)
        {
            StateManager.ChangeState(GameRef.TitleScreen);
        }

        public override void Update(GameTime gameTime)
        {
            ControlManager.Update(gameTime);
            base.Update(gameTime);
        }

        public override void Draw(GameTime gameTime)
        {
            GameRef.spriteBatch.Begin();

            GameRef.spriteBatch.Draw(background, GameRef.ScreenRectangle, Color.White);
            ControlManager.Draw(GameRef.spriteBatch);
            GameRef.spriteBatch.End();
            base.Draw(gameTime);
        }
    }
}
