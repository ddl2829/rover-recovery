﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Malibu.GameStates;
using Malibu.Controls;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Billboarding.GameScreens
{
    public class TitleScreen : BaseGameScreen
    {
        LinkLabel startLabel;
        LinkLabel help;
        SpriteFont titleFont;
        Texture2D titleBackground;

        public TitleScreen(Game1 game, GameStateManager manager)
            : base(game, manager)
        {
        
        }

        public override void Initialize()
        {
            base.Initialize();
        }

        protected override void LoadContent()
        {
            titleFont = GameRef.Content.Load<SpriteFont>(@"Fonts\TitleFont");
            titleBackground = GameRef.Content.Load<Texture2D>(@"Title");
            base.LoadContent();
            startLabel = new LinkLabel();
            startLabel.Text = "Begin Game";
            startLabel.Position = new Vector2((GameRef.ScreenRectangle.Width - startLabel.SpriteFont.MeasureString(startLabel.Text).X) / 2, 2*GameRef.ScreenRectangle.Height/3+20);
            startLabel.Color = Color.White;
            startLabel.TabStop = true;
            startLabel.HasFocus = true;
            startLabel.Selected += new EventHandler(startLabel_Selected);
            ControlManager.Add(startLabel);

            help = new LinkLabel();
            help.Text = "Controls and Instructions";
            help.Position = new Vector2((GameRef.ScreenRectangle.Width - help.SpriteFont.MeasureString(help.Text).X) / 2, startLabel.Position.Y+30);
            help.Color = Color.White;
            help.TabStop = true;
            help.HasFocus = false;
            help.Selected += new EventHandler(help_Selected);
            ControlManager.Add(help);
        }

        void help_Selected(object sender, EventArgs e)
        {
            StateManager.ChangeState(GameRef.HelpScreen);
        }

        void startLabel_Selected(object sender, EventArgs e)
        {
            StateManager.ChangeState(GameRef.ActionScreen);
        }

        public override void Update(GameTime gameTime)
        {
            ControlManager.Update(gameTime);
            base.Update(gameTime);
        }

        public override void Draw(GameTime gameTime)
        {
            GameRef.spriteBatch.Begin();
            GameRef.spriteBatch.Draw(titleBackground, GameRef.ScreenRectangle, Color.White);
            ControlManager.Draw(GameRef.spriteBatch);
            string title = "Rover Recovery";
            GameRef.spriteBatch.DrawString(titleFont, title, new Vector2((GameRef.ScreenRectangle.Width - titleFont.MeasureString(title).X) / 2, GameRef.ScreenRectangle.Height / 6), Color.White);
            base.Draw(gameTime);
            GameRef.spriteBatch.End();
        }
    }
}
