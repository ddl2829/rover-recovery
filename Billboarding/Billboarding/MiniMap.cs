using System.Collections.Generic;
using Billboarding.Components;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace Billboarding
{
    /// <summary>
    /// This is a game component that implements IUpdateable.
    /// </summary>
    public class MiniMap : Microsoft.Xna.Framework.DrawableGameComponent
    {
        Rectangle Position_Size;
        List<BillboardObject> MapObjects;
        Texture2D FrameTexture;
        Texture2D ObjectTexture;
        SpriteBatch spriteBatch;
        public Camera camera;

        public MiniMap(Game1 game, Rectangle Rect, SpriteBatch spriteBatch)
            : base(game)
        {
            Position_Size = Rect;
            this.spriteBatch = spriteBatch;

            ContentManager Content = (ContentManager)Game.Services.GetService(typeof(ContentManager));
            FrameTexture = Content.Load<Texture2D>(@"Sprites\pixel");
            ObjectTexture = FrameTexture;
        }

        /// <summary>
        /// Set the object list
        /// </summary>
        /// <param name="objects">Reference to the list of billboard objects</param>
        public void SetObjects(ref List<BillboardObject> objects)
        {
            MapObjects = objects;
        }

        /// <summary>
        /// Allows the game component to update itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);
        }

        public override void Draw(GameTime gameTime)
        {
            spriteBatch.Begin();

            spriteBatch.Draw(FrameTexture, new Rectangle(Position_Size.X - 2, Position_Size.Y - 2, Position_Size.Width + 4, Position_Size.Height + 4), Color.ForestGreen);
            spriteBatch.Draw(FrameTexture, Position_Size, Color.SandyBrown);

            foreach (BillboardObject point in MapObjects)
            {
                Vector2 mapPosition = new Vector2(Position_Size.X + point.UnitVector.X * Position_Size.Width, Position_Size.Y + point.UnitVector.Y * Position_Size.Height);
                spriteBatch.Draw(ObjectTexture, new Rectangle((int)mapPosition.X, (int)mapPosition.Y, (int)(Position_Size.Width / 20), (int)(Position_Size.Height / 20)), Color.Black);
            }

            Vector2 playerPosition = new Vector2(Position_Size.X + camera.UnitPosition.X * Position_Size.Width, Position_Size.Y + camera.UnitPosition.Y * Position_Size.Height);
            spriteBatch.Draw(ObjectTexture, new Rectangle((int)playerPosition.X, (int)playerPosition.Y, (int)(Position_Size.Width / 20), (int)(Position_Size.Height / 20)), Color.Blue);

            spriteBatch.End();

            base.Draw(gameTime);
        }
    }
}
