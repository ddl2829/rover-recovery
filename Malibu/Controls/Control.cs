﻿using System;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Malibu.Controls
{
    public abstract class Control
    {
        public string Name;
        public string Text;
        public Vector2 Size;
        public Vector2 Position;
        //public object Value;
        public bool HasFocus;
        public bool Enabled;
        public bool Visible;
        public bool TabStop;
        public SpriteFont SpriteFont;
        public Color Color;
        //public string Type;

        public event EventHandler Selected;

        public Control()
        {
            Color = Color.White;
            Enabled = true;
            Visible = true;
            SpriteFont = ControlManager.SpriteFont;
        }

        public abstract void Update(GameTime gameTime);
        public abstract void Draw(SpriteBatch spriteBatch);
        public abstract void HandleInput();

        protected virtual void OnSelected(EventArgs e)
        {
            if (Selected != null)
            {
                Selected(this, e);
            }
        }
    }
}
