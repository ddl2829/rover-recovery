using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

using Malibu.Controls;

namespace Malibu.GameStates
{
    public abstract class BaseGameScreen : GameState
    {
        protected Game GameRef;
        protected ControlManager ControlManager;

        public BaseGameScreen(Game game, GameStateManager manager) : base(game, manager)
        {
            GameRef = game;
        }

        protected override void LoadContent()
        {
            ContentManager Content = Game.Content;
            SpriteFont menuFont = Content.Load<SpriteFont>(@"Fonts\ControlFont");
            ControlManager = new ControlManager(menuFont);
            
            base.LoadContent();
        }

        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);
        }

        public override void Draw(GameTime gameTime)
        {
            base.Draw(gameTime);
        }
    }
}
