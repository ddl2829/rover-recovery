using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

namespace Malibu.Input
{
    public class InputHandler : Microsoft.Xna.Framework.GameComponent
    {
        static KeyboardState keyboardState;
        static KeyboardState lastKeyboardState;

        static MouseState mouseState;
        static MouseState lastMouseState;

        public static KeyboardState KeyboardState
        {
            get { return keyboardState; }
        }

        public static KeyboardState LastKeyboardState
        {
            get { return lastKeyboardState; }
        }

        public InputHandler(Game game)
            : base(game)
        {
            mouseState = Mouse.GetState();
            keyboardState = Keyboard.GetState();
            Mouse.SetPosition(300, 300);
        }

        public override void Initialize()
        {
            base.Initialize();
        }


        public override void Update(GameTime gameTime)
        {
            lastKeyboardState = keyboardState;
            keyboardState = Keyboard.GetState();

            lastMouseState = mouseState;
            mouseState = Mouse.GetState();
            
            base.Update(gameTime);
        }

        public static void Flush()
        {
            lastKeyboardState = keyboardState;
            lastMouseState = mouseState;
        }

        public static bool KeyReleased(Keys key)
        {
            return keyboardState.IsKeyUp(key) && lastKeyboardState.IsKeyDown(key);
        }

        public static bool KeyPressed(Keys key)
        {
            return keyboardState.IsKeyDown(key) && lastKeyboardState.IsKeyUp(key);
        }

        public static bool KeyDown(Keys key)
        {
            return keyboardState.IsKeyDown(key);
        }

        public static bool MouseLeftClicked()
        {
            return (mouseState.LeftButton == ButtonState.Pressed) && (lastMouseState.LeftButton == ButtonState.Released);
        }

        public static Vector2 MousePosition()
        {
            return new Vector2(mouseState.X, mouseState.Y);
        }

        public static Vector2 MouseMotion()
        {
            Vector2 motion = new Vector2(mouseState.X - lastMouseState.X, mouseState.Y - lastMouseState.Y);
            motion.Normalize();
            return motion;
        }

        public static Vector2 MouseMotionFromLock()
        {
            return new Vector2(mouseState.X - 300, mouseState.Y - 200);
        }

        public static bool MouseMoved()
        {
            return lastMouseState.X != mouseState.X;
        }

        public static void LockMouse()
        {
            Mouse.SetPosition(300, 300);
        }
    }
}
